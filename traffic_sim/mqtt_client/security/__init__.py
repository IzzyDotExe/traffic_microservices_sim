from pathlib import Path
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.exceptions import InvalidSignature
from codecs import decode

def generate_key_pair():
  key_size = 2048  # Should be at least 2048

  private_key = rsa.generate_private_key(
      public_exponent=65537,  # Do not change
      key_size=key_size,
  )

  public_key = private_key.public_key()
  return private_key, public_key

def generate_and_save_key_pair(password):
  private_key, public_key = generate_key_pair()
  store_public_key(public_key, "public.pem")
  store_private_key(private_key, password, "private.pem")

def store_private_key(private_key, password, file):
  pwrd = password.encode("utf-8")
  key_pem_bytes = private_key.private_bytes(
    encoding=serialization.Encoding.PEM,  # PEM Format is specified
    format=serialization.PrivateFormat.PKCS8,
    encryption_algorithm=serialization.BestAvailableEncryption(pwrd),
  )

  # Filename could be anything
  key_pem_path = Path(file)
  key_pem_path.write_bytes(key_pem_bytes);

def store_public_key(public_key, file):

  public_pem_bytes = public_key.public_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PublicFormat.SubjectPublicKeyInfo,
  )

  # Filename could be anything
  public_pem_path = Path(file)
  public_pem_path.write_bytes(public_pem_bytes);

def load_public_keys(string):
  key_bytes = decode(string, "hex")
  key_pem = serialization.load_pem_public_key(key_bytes)
  return key_pem

def load_private_key(file, password):
  # check if file exist, if not -> generate a new private key and return it
  private_pem_bytes = Path(file).read_bytes()
  
  private_key_from_pem = serialization.load_pem_private_key(
      private_pem_bytes,
      password=password.encode('utf-8'),
  )
  return private_key_from_pem

def load_public_key(file):
  # verify file exist
  public_pem_bytes = Path(file).read_bytes()
  
  public_key_from_pem = serialization.load_pem_public_key(public_pem_bytes)
  
  return public_key_from_pem

def sign_message(message, private_key):
  return private_key.sign(
    message.encode("utf-8"),
    padding.PSS(
      mgf=padding.MGF1(hashes.SHA256()),
      salt_length=padding.PSS.MAX_LENGTH
    ),
    hashes.SHA256()
  )
  
def verify_message_signature(signature, message, public_key):
  try:
    public_key.verify(
        signature,
        message.encode("utf-8"),
        padding.PSS(
          mgf=padding.MGF1(hashes.SHA256()),
          salt_length=padding.PSS.MAX_LENGTH
        ),
        hashes.SHA256()
    )
    return True
  except InvalidSignature:
    return False

