from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger

from resources import Weather

API_SPEC_URL = '/swagger'

app = Flask(__name__)
api = swagger.docs(Api(app), '1.0.0', api_spec_url=API_SPEC_URL, swaggerVersion="2.0")
api.add_resource(Weather, '/api/weather')

if __name__ == '__main__':
  app.run('0.0.0.0', '80')
