import pytest
from traffic_sim.mqtt_client.jwt_wrapper import *
import time

@pytest.fixture()
def test_generate_token():
  return authenticate("admin", "password")

def test_auth(test_generate_token):
  expired = check_expired(decode(test_generate_token))
  assert not expired
  
@pytest.fixture
def test_gen_token_expired():
  return authenticate("admin", "password", 1)

def test_auth_fail(test_gen_token_expired):
  time.sleep(1)
  expired = check_expired(decode(test_gen_token_expired))
  assert expired