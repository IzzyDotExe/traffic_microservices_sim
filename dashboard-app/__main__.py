from flask import Flask, render_template
import requests

app = Flask(__name__)
API_HOST = '142.117.180.199'
#'localhost'
# 

@app.route('/', methods=['GET'])
def home():
  r1 = requests.get(f'http://{API_HOST}:8001/api/motion?postal_code=A4L5J3')
  r2 = requests.get(f'http://{API_HOST}:8000/api/weather?postal_code=A4L5J3')
  
  motion_data= r1.json()
  weather_data= r2.json()
  return render_template('index.html', weather_data=weather_data, motion_data=motion_data)

if __name__ == '__main__':
  app.run('localhost', '80')