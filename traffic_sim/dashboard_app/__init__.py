from flask import Flask, render_template
import requests

def run(data_collector):
  data_collector.client.client.loop_start()
  app = Flask(__name__)
  
  @app.route('/', methods=['GET'])
  def home():
    
    clients_list = [str(x) for x in data_collector.collection.keys()]
    
    try:
      data_list = data_collector.collection[clients_list[0]]
    except:
      data_list = []
      
    try:
      data_list_2 = data_collector.collection[clients_list[1]]
    except:
      data_list_2 = []
    
    return render_template('index.html', data_list_c1=data_list, data_list_c2=data_list_2)

  app.run('localhost', '80', debug=False)
