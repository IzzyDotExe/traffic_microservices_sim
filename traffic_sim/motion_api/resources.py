from flask_restful import Resource
from flask_restful_swagger import swagger
from flask import request, abort
from random import Random
from datetime import datetime

from marshmallow import Schema, fields
from marshmallow.validate import OneOf

from jwt_wrapper import decode, check_expired

VALID_POSTCODES = ['A4L5J3', 'M9A1A8','H3L5S3', 'C7M7J3', 'D8C1K5']

class MotionDataQuerySchema(Schema):
  postal_code=fields.Str(required=True, validate=OneOf(VALID_POSTCODES))

class MotionData(Resource):
  
  def __init__(self):
    self.rand = Random()
    self.VALID_POSTCODES = VALID_POSTCODES
    self.VALID_DETECTIONS = ['motion', 'colision']
    self.schema = MotionDataQuerySchema()
    
  @swagger.operation(
    notes='This motion data is mocked! Meaning it is completley random and not representative of any sensor and any location.',
    parameters=[
      {
        "name": "postcal_code",
        "description": "the postal code you want to look for colision in.",
        "required": True,
        "dataType": "string",
        "allowMultiple": False
      }
    ],
    responseMessages=[
      {
        "code": 200,
        "message": "Sucessfully fetched the motion data from the postal code.",
      },
      {
        "code": 400,
        "message": "You have invalid query parameters."
      }
    ]
  )
  def get(self):
    
    errors = self.schema.validate(request.args)

    if errors:
      abort(400, str(errors))
    
    # checking if token is valid
    try:
      if request.headers['Authorization']:
        token = request.headers['Authorization'].split(' ')[1]
        isTokenInvalid = check_expired(decode(token))
        
        if isTokenInvalid:
          abort(401, {'error': 'Expired token'})
      else:
        abort(400, {'error': 'Missing authorization header'})
    except KeyError:
      abort(400, {'error': 'Missing authorization header'})
    randdec = self.rand.randint(0, len(self.VALID_DETECTIONS)-1)
    detect = self.VALID_DETECTIONS[randdec]

    result = {
      'code': 200,
      'postal_code': request.args['postal_code'],
      'detection': {
        'type': detect,
        'value': True
      },
      'datetime': datetime.utcnow().strftime('%d/%m/%Y - %H:%M:%S')
    }

    return result
    
