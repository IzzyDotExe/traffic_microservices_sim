import time
from camera import CameraObj
from threading import Thread
from datetime import datetime
from mqtt_client.client import MQTTClient
from mqtt_client.CONFIG import POSTAL_CODE, API_HOST, MQTT_USER, MQTT_PWD
import json 
from mqtt_client.jwt_wrapper import authenticate
import requests

class TrafficSimThread:
    def __init__(self, jwt_expired, delay=0.001):
        self.jwt_expired  = jwt_expired
        self.time_to_sleep = delay
        self.mqtt_client = MQTTClient()
        self.mqtt_client.client.loop_start()
        self.mqtt_client.client.loop_stop()
        
    def getWeatherData(self):

        token = self.mqtt_client.token
        
        header = {'Authorization': 'Bearer '+ str(token)}
        req = requests.get(f'http://{API_HOST}:8000/api/weather?postal_code={POSTAL_CODE}', headers=header)
        
        if not req.ok:
            if req.status_code == 401:
                self.jwt_expired.set()

        weather_data = req.json()
        
        return weather_data

        
    # When the sensor picks up movement while the light is red, offense is logged in a text file
    def log_offence(self, img, data, time):
        self.mqtt_client.client.loop_start()
        data = {
            "motion": {
                "code": 200,
                "postal_code": POSTAL_CODE,
                "detection": {
                    "type" : data,
                    "value": True
                },
                "datetime": str(time)
            },
            "weather": self.getWeatherData(),
            "image": img
        }
        self.mqtt_client.publishData("data", json.dumps(data))
        self.mqtt_client.client.loop_stop()
    
    # Checks both the traffic light and the sensor to see if in the simulation a car ran a red light
    def runSimulation(self, movement_detected, colision_detected, light_red, camera, end_program, jwt_expired):
        while True:
            
            if jwt_expired.is_set():
                self.mqtt_client.regen_token()
            
            time.sleep(self.time_to_sleep)
    
            if end_program.is_set():
                return
            if movement_detected.is_set():
                movement_detected.clear()
                if light_red.is_set():
                  light_red.clear()
                  cam = CameraObj(camera)
                  img = cam.snapShot()    
                  self.log_offence(img, "motion", datetime.utcnow())
                  
            if colision_detected.is_set():
                colision_detected.clear()
                cam = CameraObj(camera)
                img = cam.snapShot()
                self.log_offence(img, "colision", datetime.utcnow())
            else:
                continue
