from .client import MQTTClient
import json

class MqtDataCollector:
  
  def __init__(self):
    self.client = MQTTClient(client_id="dashboard2")
    
    def topic_handler(a, b, c, msg):
      self.addToCollection(msg)
      
    self.client.add_topic_handler("data", topic_handler)
    self.collection = {}
  
  def addToCollection(self, item):

    if not item["client_id"] in [str(x) for x in self.collection.keys()]:
      self.collection[item["client_id"]] = []
      
    self.collection[item["client_id"]].append(json.loads(item["data"]))
