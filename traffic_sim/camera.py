import time
from datetime import datetime as dt
import asyncio

class Camera:
  # Define camera object
  def __init__(self):     
    self.camera = None
    self.camera_config = None

  def take_picture(self):
    return
  
  # Loop for how many pictures are taken
  def take_pictures(self):
    return
  
class CameraMock(Camera):

  """
  Same as the camera class except it skips actually taking the picture.
  """

  # Takes pictures and saves them as the date in output folder
  def take_picture(self):
    now = dt.now()
    return "./output/"+str(now)+".jpg"

  # Loop for how many pictures are taken
  def take_pictures(self):
    num = 0
    pics = []
    while num < 5:
      pics.append(self.take_picture())
      num+=1
      
    return pics
       
class PiCamera(Camera):
  
  # Define camera object
  def __init__(self):     
    from picamera2 import Picamera2 as picam
    self.camera = picam()
    self.camera_config = self.camera.create_preview_configuration()

  # Takes pictures and saves them as the date in output folder
  def take_picture(self):
    self.camera.start()
    now = dt.now()

    self.camera.capture_file("./output/"+str(now)+".jpg")
    return "./output/"+str(now)+".jpg"
            
class CameraObj:
  
  def __init__(self, camera: Camera):
    self.camera = camera
    
  # Takes pictures and returns last output image.
  def snapShot(self):
    pics = self.camera.take_picture()
    return pics
