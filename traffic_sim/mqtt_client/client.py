from gc import callbacks
import json
import paho.mqtt.client as mqtt 
from codecs import decode
from .jwt_wrapper import authenticate
from .security import load_private_key, sign_message, store_public_key, load_public_key, verify_message_signature, load_public_keys
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat
from .CONFIG import *

class MQTTClient():
  
    def __init__(self, client_id=MQTT_ID, username=MQTT_USER, passwd=MQTT_PWD, broker_hostname=BROKER_HOST, port=MQTT_PORT, private_key=PRIVATE_KEY):
        
      # add data validation for id -> env_variable(cliend_id, username, pwd) for each rrpi
      # check if key exist
      
      # variable/instance properties
      self.client_id = client_id
      self.username = username
      self.password = passwd
      self.broker_hostname = broker_hostname
      self.port = port
      
      # pub key to encrypt msg, private to decrypt
      self.private_key = load_private_key(private_key, self.password)
      self.public_key = self.private_key.public_key()
      
      self.token = self.regen_token()
      self.client = mqtt.Client(client_id=self.client_id, userdata=None)
      
      # callback functions
      def on_conn(client, userdata, flags, return_code):
        self.on_connect(client, userdata, flags, return_code)
    
      def on_msg(client, userdata, message):
        self.on_message(client, userdata, message)

      self.client.on_connect = on_conn
      self.client.on_message = on_msg
      
      # change with your user and password auth
      self.client.username_pw_set(username=self.username, password=self.password)     
      self.client.connect(self.broker_hostname, self.port, 60)    
      
      self.topic_handlers = {}

    def regen_token(self):
      self.token = authenticate('admin', 'password', 3600)

    def add_topic_handler(self, topic, handler):
      self.client.subscribe(topic)
      self.topic_handlers[topic] = handler
    
    def on_connect(self, client, userdata, flags, return_code):
        print("CONNACK received with code %s." % return_code)
        if return_code == 0:
            print("connected")
            # publish own key and subscribe to other client public key
            print("public key published!")
            client.publish(topic='public key', payload=json.dumps({"public_key":self.public_key.public_bytes(encoding=Encoding.PEM, format=PublicFormat.SubjectPublicKeyInfo).hex(), "client_id": self.client_id}))
            client.subscribe('public key')
            
        else:
            print("could not connect, return code:", return_code)

    def on_message(self, client, userdata, message ):
        msg_payload_json = json.loads(message.payload.decode('utf-8'))
        # print(msg_payload_json)
        
        #check which topic is the message related to
        if message.topic == 'public key':
            ## decode other client public key and store as pem file (client2_public_key)
            key = load_public_keys(msg_payload_json["public_key"])
            store_public_key(public_key=key, file=f'{msg_payload_json["client_id"]}_key.pem')
            print(f'Recieved public key from {msg_payload_json["client_id"]}')
            
        else:
            # extract signature from message payload and use message in payload to verify signature
            # only process message if verification pass -> add message to message_received, else discard message
            other_publickey = load_public_key(f'{msg_payload_json["client_id"]}_key.pem')
            data = msg_payload_json['data']
            signature = decode(msg_payload_json['signature'], "hex")
            if verify_message_signature(signature, data, other_publickey):
                if message.topic in self.topic_handlers.keys():
                    self.topic_handlers[message.topic](client, userdata, message, msg_payload_json)
  
    # function to publish weather or motion data
    def publishData(self, topic: str, data: str):
        # topic = "Weather"
        # all message payload except 1st one should digitally signed with key and add signature to payload
        signature = sign_message(data, self.private_key).hex()
        payload = json.dumps({'client_id': self.client_id, 'data': data, 'signature': signature})
        
        result = self.client.publish(topic=topic, payload=payload)

        if result.is_published:
            print("Message was published to topic " + topic)
      
        else:
            print("Failed to send message to topic " + topic)
