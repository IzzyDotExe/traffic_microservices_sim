from threading import Thread, Event
import time
from camera import Camera, CameraMock, PiCamera
from datetime import datetime as dt

from movementsensor import MovementSensorThread
from trafficsim import TrafficSimThread
from light import LightThread

from mqtt_client.CONFIG import API_HOST, POSTAL_CODE

movement_detected = Event()
colision_detected = Event()
jwt_expired = Event()
light_red = Event()
end_program = Event()

def main():

    end_program.clear()

    # Set up all the threads for the program to run (Light, Camera, Sensor, TrafficLight)
    light = LightThread(2)
    lightThread = Thread(target=light.runSimulation, args=(light_red, end_program ))

    trafficSim = TrafficSimThread(0.5)
    trafficSim.mqtt_client.regen_token()
    camera = CameraMock()
    simThread = Thread(target=trafficSim.runSimulation, args=(movement_detected, colision_detected, light_red, camera, end_program, jwt_expired))
    
    movementSensor = MovementSensorThread(f'http://{API_HOST}:8001/api/motion?postal_code={POSTAL_CODE}', trafficSim.mqtt_client.token)
    movementSensorThread = Thread(target=movementSensor.detectMovement, args=(movement_detected, colision_detected, end_program, jwt_expired))
    
    movementSensorThread.start()
    simThread.start()
    lightThread.start()
    
    movementSensorThread.join()
    simThread.join()
    lightThread.join()

# Stops everything when you press CTRL c
def destroy():
    
  end_program.set()
  print("Finishing Traffic Light simulation...")

if __name__ == '__main__':     # Program entrance
    print ('Program is starting ... ')
    try:
        main()
    except KeyboardInterrupt:  # Press ctrl-c to end the program.
        destroy()
