from random import Random
import datetime
from flask import request, abort, jsonify
from flask_restful import Resource
from flask_restful_swagger import swagger

from marshmallow import Schema, fields
from marshmallow.validate import OneOf

from jwt_wrapper import check_expired, decode

postal_codes = ['A4L5J3', 'M9A1A8','H3L5S3', 'C7M7J3', 'D8C1K5']

class WeatherQuerySchema(Schema):
  postal_code=fields.Str(required=True, validate=OneOf(postal_codes))

class Weather(Resource):
  
  def __init__(self):
    self.random = Random()
    self.schema = WeatherQuerySchema()
    self.conditions = { 'type': ['Snowfall', 'Rain', 'Sunny', 'Cloudy'], 'intensity': ['Heavy', 'Medium', 'Light']}  
    
  @swagger.operation(
    notes='This weather data is mocked! Meaning its values are completly random and not accurate to the specified location.',
    parameters=[
      {
        "name": "postal_code",
        "description": "Postal code get weather information",
        "required": True,
        "dataType": "string",
        "allowMultiple": False
      }
    ],
    responseMessages=[
      {
        "code": 200,
        "message": "Successfully fetched the weather data for the specified postal code.",
      },
      {
        "code": 400,
        "message": "You have invalid query parameters."
      }
    ]
  )
  def get(self):
    errors = self.schema.validate(request.args)

    if errors:
      abort(400, str(errors))
    
    try:
      # checking if token is valid
      if request.headers['Authorization']:
        token = request.headers['Authorization'].split(' ')[1]
        isTokenInvalid = check_expired(decode(token))
        # check_expired( decode(token))
        
        if isTokenInvalid:
          abort(401, {'error': 'Expired token'})
      else:
        abort(400, {'error': 'Missing authorization header'})
    except KeyError:
      abort(400, {'error': 'Missing authorization header'})
    # return an address with all the fields (temperature, conditions, date)
    postal_code = request.args['postal_code']
    weather_type = self.random.choice(self.conditions['type'])
    
    # default is n/a or random intensity if its snowfal or rain
    intensity = 'n/a' if weather_type == 'Sunny' or weather_type == 'Cloudy' else self.random.choice(self.conditions['intensity'])
    date = datetime.datetime.utcnow()
    dateStr = date.strftime('%A, %d %B')
    timeStr = date.strftime('%I:%M %p')
    
    # random value between 0 & -40 if the weather condition is snowfall
    temperature = self.random.randint(0, 40)*-1 if weather_type == 'Snowfall' else self.random.randint(0, 40)

    info = { 'code': 200,
             'postal_code': postal_code, 
             'temperature': temperature, 
             'condition': { 'type': weather_type, 'intensity': intensity }, 
             'datetime': [dateStr, timeStr] }
    
    return jsonify(info)
