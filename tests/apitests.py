import pytest
import traffic_sim.mqtt_client.jwt_wrapper as jwt
import time

import requests

@pytest.fixture()
def define_consts():
  API_TEST_HOST = '0.0.0.0'
  API_MOTION_PORT = '8001'
  API_WEATHER_PORT = '8000'

  POST_CODES = ['A4L5J3', 'M9A1A8','H3L5S3', 'C7M7J3', 'D8C1K5']
  return API_MOTION_PORT, API_TEST_HOST, API_WEATHER_PORT, POST_CODES

@pytest.fixture
def test_setup_request():
  
  # Setup the jwt token
  token = jwt.authenticate('admin', 'password')  
  
  # Create the authorization header
  header = {'Authorization': 'Bearer '+ str(token)}

  return token, header

@pytest.fixture
def test_setup_invalid_jwt():
  
  # Setup the jwt token
  token = jwt.authenticate('admin', 'badpassword')  
  
  # Create the authorization header
  header = {'Authorization': 'Bearer '+ str(token)}

  return token, header

@pytest.fixture
def test_setup_expired_jwt():
  
  # Setup the jwt token
  token = jwt.authenticate('admin', 'password', 1)  
  
  # Create the authorization header
  header = {'Authorization': 'Bearer '+ str(token)}

  return token, header

def test_weather_api(test_setup_request, define_consts):
  
  API_MOTION_PORT, API_TEST_HOST, API_WEATHER_PORT, POST_CODES = define_consts
  token, header = test_setup_request
  
  for postal_code in POST_CODES:
    request = requests.get(f'http://{API_TEST_HOST}:{API_WEATHER_PORT}/api/weather?postal_code={postal_code}', headers=header)
    assert request.ok
    
    json = request.json()
    
    assert json['code'] == 200
    assert json['postal_code'] == postal_code

def test_weather_api_invalid_route(test_setup_request, define_consts, test_setup_expired_jwt, test_setup_invalid_jwt):
  
  API_MOTION_PORT, API_TEST_HOST, API_WEATHER_PORT, POST_CODES = define_consts
  token, header = test_setup_request
  
  for postal_code in POST_CODES:
    
    request = requests.get(f'http://{API_TEST_HOST}:{API_WEATHER_PORT}/api/weather/invalid/route?postal_code={postal_code}', headers=header)
    assert not request.ok
    assert request.status_code == 404
    
def test_weather_api_expired(define_consts, test_setup_expired_jwt):
  
  API_MOTION_PORT, API_TEST_HOST, API_WEATHER_PORT, POST_CODES = define_consts
  tokenexpired, headerexpired = test_setup_expired_jwt
  
  time.sleep(2)
  
  for postal_code in POST_CODES:

    request = requests.get(f'http://{API_TEST_HOST}:{API_WEATHER_PORT}/api/weather?postal_code={postal_code}', headers=headerexpired)
    assert not request.ok
    assert request.status_code == 401
    
def test_weather_api_invalid(define_consts, test_setup_invalid_jwt):
  
  API_MOTION_PORT, API_TEST_HOST, API_WEATHER_PORT, POST_CODES = define_consts
  tokeninvalid, headerinvalid = test_setup_invalid_jwt
  
  for postal_code in POST_CODES:
    
    request = requests.get(f'http://{API_TEST_HOST}:{API_WEATHER_PORT}/api/weather?postal_code={postal_code}', headers=headerinvalid)
    assert not request.ok
    assert request.status_code == 500
    
def test_motion_api(test_setup_request, define_consts):
  API_MOTION_PORT, API_TEST_HOST, API_WEATHER_PORT, POST_CODES = define_consts
  token, header = test_setup_request
  
  for postal_code in POST_CODES:
    
    request = requests.get(f'http://{API_TEST_HOST}:{API_MOTION_PORT}/api/motion?postal_code={postal_code}', headers=header)
    
    assert request.ok
    
    json = request.json()
    
    assert json['code'] == 200
    assert json['postal_code'] == postal_code
   
def test_motion_api_invalid_route(test_setup_request, define_consts, test_setup_expired_jwt, test_setup_invalid_jwt):
  
  API_MOTION_PORT, API_TEST_HOST, API_WEATHER_PORT, POST_CODES = define_consts
  token, header = test_setup_request
  
  for postal_code in POST_CODES:
    
    request = requests.get(f'http://{API_TEST_HOST}:{API_MOTION_PORT}/api/motion/invalid/route?postal_code={postal_code}', headers=header)
    assert not request.ok
    assert request.status_code == 404
    
def test_motion_api_expired(define_consts, test_setup_expired_jwt):
  
  API_MOTION_PORT, API_TEST_HOST, API_WEATHER_PORT, POST_CODES = define_consts
  tokenexpired, headerexpired = test_setup_expired_jwt
  
  time.sleep(2)
  
  for postal_code in POST_CODES:

    request = requests.get(f'http://{API_TEST_HOST}:{API_MOTION_PORT}/api/motion?postal_code={postal_code}', headers=headerexpired)
    assert not request.ok
    assert request.status_code == 401
    
def test_motion_api_invalid(define_consts, test_setup_invalid_jwt):
  
  API_MOTION_PORT, API_TEST_HOST, API_WEATHER_PORT, POST_CODES = define_consts
  tokeninvalid, headerinvalid = test_setup_invalid_jwt
  
  for postal_code in POST_CODES:
    
    request = requests.get(f'http://{API_TEST_HOST}:{API_MOTION_PORT}/api/motion?postal_code={postal_code}', headers=headerinvalid)
    assert not request.ok
    assert request.status_code == 500
    