import time
import requests

class MovementSensorThread:
    def __init__(self, api_req_uri, token, delay=1):
        self.time_to_sleep = delay
        self.req_uri = api_req_uri
        self.token = token
    
    # Checks if movement is detected with the sensor
    def detectMovement(self, movement_detected, colision_detected, end_program, jwt_expired):
        while True:
            
            time.sleep(self.time_to_sleep)
    
            header = {'Authorization': 'Bearer '+ str(self.token)}
            req = requests.get(self.req_uri, headers=header)

            if not req.ok:
                if req.status_code == 401:
                    jwt_expired.set()
                    continue
            
            req = req.json()
            
            if end_program.is_set():
                return
            
            if req['detection']['type'] == 'motion' and req['detection']['value'] == True:
                movement_detected.set()
            elif req['detection']['type'] == 'colision' and req['detection']['value'] == True:
                colision_detected.set()