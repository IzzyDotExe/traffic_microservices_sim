# traffic_microservices_sim
by Israel Aristide & Fred Louis
# Features and capabilites

Some of the key technical features are: 

- code is designed to be easily reused or redesigned
- Docker compose support for easy microservice deployment
- JWT api authentication
- Message signing for security in communication with the mqtt broker.

![Diagram](./Diagram.jpg)

## Description

This is a fully featured IOT systems simulation featuring asymeytic key based signing, JWT authentication, MQTT communication, and more!

Complete with a dashboard to visualize all the data.

The full guide to running the sim is below!

## Setup

### Microservices

The two web microservices (motion and weather apis) and the MQTT broker are all run through DOCKER COMPOSE. 

You can simply pull the git repository and run

```bash
docker compose up -d
```

This command will build and run two containers which contain the APIs.
> NOTE: The weather API will be running on port **8000** and the motion API on port **8001** by default. You can edit the ports if you'd like by going into the docker-compose.yml and changing those two values.

### Dashboard

run the dashboard on any computer by first installing the requirements


```sh
python3 -m pip install -r requirements.txt
```


Then, you should generate your keys

```sh
python3 traffic_sim/mqtt_client/keygen.py
```

The password for the private key is the MQTT_PWD from the ``traffic_sim/mqtt_client/CONFIG.py``

Then run the dashboard

```sh
python3 traffic_sim/run_dashboard.py
```


### IOT Traffic Sim

The traffic sim system which runs on each IOT device (raspberry pi) is run directly through python. 

First you shoud go to the ``traffic_sim/mqtt_client/CONFIG.py`` File and re-setup your configuration.

```python
# CONFIG.py

BROKER_HOST='ip' # You can change to whichever machine you started the docker microservices on.
MQTT_ID="client1" # Make sure this is unique
MQTT_USER="user1" # Username for mqtt login
MQTT_PWD="password" # pwrd for mqtt login
MQTT_PORT=1883 # You probably should leave this default
PRIVATE_KEY="private.pem" # change this to the name of the private key you have
POSTAL_CODE="A4L5J3" # Postal code you are operating from in the simuation
API_HOST='ip' # change to where you ran your docker compose microservices.

```

After you've configured the project on each node, run the following commands on them

```sh
python3 -m pip install -r requirements.txt
# run this if you're using raspberry pi and using the camera
python3 -m pip inttall -r requirements-pi.txt
```

If you are using raspberry pi and want to use the real camera you should change this line 27 in the ``traffic_sim/__main__.py`` file

```python
...

26    trafficLight = TrafficSimThread(0.5)
27    # camera = CameraMock()
28    camera = PiCamera() 

...
```

finally run the simulation 

```sh
python3 traffic_sim
```

