import time

class LightThread:
  def __init__(self, delay=2):
    self.time_to_sleep = delay
    self.color = []
    
  def setColor(self, r_val,g_val,b_val):      # change duty cycle for three pins to r_val,g_val,b_val
    self.color = [r_val, g_val, b_val]

  # Light switches colors at an interval to simulate traffic light
  def runSimulation(self, light_red, end_program):
    cycle = 0
    
    while True:
      if (end_program.is_set()):
        return
      
      colors = [0, 0, 0]
      cycle += 1
      time.sleep(self.time_to_sleep)
      
      colors[cycle%len(colors)] = 100
      
      #Check if color is red
      if cycle%len(colors) == 1:
        light_red.set()
        # print("Traffic light is red")
      else:
        # print("Traffic light is not red")
        light_red.clear()
        
      self.setColor(colors[0], colors[1], colors[2])
