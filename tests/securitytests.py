"""
def generate_key_pair():
def generate_and_save_key_pair(password):
def store_private_key(private_key, password, file):
def store_public_key(public_key, file):
def load_private_key(file, password):
def load_public_key(file):
def encrypt_message(message, public_key):
def decrpt_message(message_encrypted, private_key):
def sign_message(message, private_key):
def verify_message_signature(signature, message, public_key):
"""
import os
import pytest
from traffic_sim.mqtt_client.security import *

def get_test_private_key_password():
  return "password1234"

@pytest.fixture()
def test_keygen():
  return generate_key_pair()

@pytest.fixture
def test_signature(test_keygen):
  test_keygen_priv, test_keygen_pub = test_keygen
  signature = sign_message("message", test_keygen_priv)
  return signature

def test_keys_match(test_keygen):
  test_keygen_priv, test_keygen_pub = test_keygen
  assert test_keygen_priv.public_key() == test_keygen_pub

def test_save_keys(test_keygen):
  test_keygen_priv, test_keygen_pub = test_keygen
  store_private_key(test_keygen_priv, get_test_private_key_password(), "private.pem")
  store_public_key(test_keygen_pub, "public.pem")
  assert os.path.exists("private.pem") and os.path.exists("public.pem")
  
def test_signature_works(test_keygen, test_signature):
  test_keygen_priv, test_keygen_pub = test_keygen
  assert test_signature is not None and verify_message_signature(test_signature, "message", test_keygen_pub)
  
def test_signature_works_rev(test_keygen, test_signature):
  test_keygen_priv, test_keygen_pub = test_keygen
  assert test_signature is not None and not verify_message_signature(test_signature, "wrong message", test_keygen_pub)
